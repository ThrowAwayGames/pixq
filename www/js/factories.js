 /**
 * The Favourites factory handles saving and loading favourites
 * from local storage, and also lets us save and load the
 * last active favourite index.
 */
var factories = {};
var services = {};
factories.newImages = function($http, $q) {
	//ajax requests
	
	var newImg = {};
	
	var endDate = {};
	
	var startDate = {};
	
	var updateDb = false;
	
	
	newImg.setStartDate = function(Favourites, _screen) { //current page when downloading image data
		startDate[_screen] = Favourites.getStartDate(_screen);
	}
	newImg.setEndDate = function(_date, _screen) { //current page when downloading image data
		endDate[_screen] = _date;
	}
	newImg.getStartDate = function(_screen) {
		return startDate[_screen];
	}
	newImg.getEndDate = function(_screen) {
		return endDate[_screen];
	}
	newImg.getDates = function() {
		console.log(startDate);
	}
	newImg.setUpdateDb = function() {
		updateDb = true;
	}
	var getUpdateDb = function() {
		if (updateDb) 
		{
			updateDb = false;
			return true;
		}
	}
	
	newImg.getStartDate = function(_screen) {
		return startDate[_screen];
	}
	
	newImg.getImages = function(_screen, userData) {
		var deferred = $q.defer();
		
		console.log(startDate[_screen], endDate[_screen]);
		//updateDb = false;
		
		$http.get("http://app.pixq.co:9001", {
			//method: "GET",
			headers: {
				Authorization: "Client-ID affe3698b8c8e73",
				Accept: 'application/json'
			},
			params: {
				order: sources['imgur'][_screen],
				date: getUpdateDb() ? startDate[_screen] : endDate[_screen]
			}
		
		}).success(function(data, status, headers, config) {

			//if you don't get any images, reset start date to 0
			if (!data.length) 
			{
				newImg.setUpdateDb();
				angular.element(document.querySelector(".ion-refresh." + _screen)).addClass("visible");
				userData.setStartDate(0);
				newImg.setStartDate(userData, _screen);
				deferred.reject("Warn: no images " + status);
				return;
			}
			angular.element(document.querySelector(".ion-refresh." + _screen)).removeClass("visible");
			
			deferred.resolve(data);
		
		
		}).error(function(data, status, headers, config) {
			deferred.reject("Warn: no images " + status);
		})
		
		return deferred.promise;
	
	}
	
	
	return newImg;
}
factories.userData = function($http, $q) {
	delete window.localStorage['lastActiveFavourite'];
	return {
		all: function() {
			if (window.localStorage['favs'] == undefined)
				window.localStorage['favs'] = '';
			var favString = window.localStorage['favs'];
			if (favString) {
				return angular.fromJson(favString);
			}
			return [];
		},
		save: function(_favourites) {
			window.localStorage['favs'] = angular.toJson(_favourites);
		},
		setStartDate: function(_screen, _start) {
			if (window.localStorage['startDate'] == undefined)
				window.localStorage['startDate'] = "{}";
			var startDates = JSON.parse(window.localStorage['startDate']);
			startDates[_screen] = _start;
			window.localStorage['startDate'] = JSON.stringify(startDates);
		},
		setActiveDate: function(_screen, _end) {
			if (window.localStorage['activeDate'] == undefined)
				window.localStorage['activeDate'] = "{}";			
			var activeDates = JSON.parse(window.localStorage['activeDate']);
			activeDates[_screen] = _end;
			window.localStorage['activeDate'] = JSON.stringify(activeDates);
		},
		getStartDate: function(_screen) {
			if (window.localStorage['startDate'] == undefined)
				return 0;
			return parseInt(JSON.parse(window.localStorage['startDate'])[_screen]) || 0;
		},
		getActiveDate: function(_screen) {
			if (window.localStorage['activeDate'] == undefined)
				return 0;
			return parseInt(JSON.parse(window.localStorage['activeDate'])[_screen]) || 0;
		},
		
		newFavourite: function(_image) {
			var deferred = $q.defer();
			
			var fav = {
				_id: _image._id,
				title: _image.title,
				thumb: sources.imgur.imgUrl + _image.id + "s.jpg",
				cardImage: _image.cardImage,
				image: _image.image, //"http://i.imgur.com/"+image.id+"l.jpg",
				thumb: _image.thumb,
				description: _image.description,
				source: _image.source,
				datetime: _image.datetime,
			};
			
			if (window.localStorage['favs'].lastIndexOf(fav._id) >= 0)
				deferred.reject();
			
			
			$http.post("http://app.pixq.co:9001/" + _image._id, {
			/*headers: {
					Authorization: "Client-ID affe3698b8c8e73",
					Accept: 'application/json'
				},*/
			
			}).success(function(data, status, headers, config) {
				deferred.resolve(fav);
			}).error(function(data, status, headers, config) {
				console.log(status);
			});
			
			
			
			return deferred.promise;
		},
		getLastActiveFavourite: function() {
			return parseInt(window.localStorage['lastActiveFavourite']) || 0;
		},
		setLastActiveFavourite: function(index) {
			window.localStorage['lastActiveFavourite'] = index;
		}
	}
}


factories.pageTracker = function() {
	var tracker = {};
	
	tracker.newRun = true;
	tracker.id = Math.random();
	tracker.viewName = "newImg";
	tracker.images = {};
	tracker.firstLoad = {};
	tracker.imageInfo = {};
	
	tracker.setFirstLoad = function(_set) {
		tracker.firstLoad[tracker.viewName] = _set;
	}
	
	tracker.getFirstLoad = function(_viewName) {
		if (_viewName)
			return tracker.firstLoad[_viewName];
		else
			return tracker.firstLoad[tracker.viewName];
	}
	
	
	tracker.setImages = function(_images) {
		tracker.images[tracker.viewName] = _images;
	}
	
	tracker.getImages = function(_viewName) {
		if (_viewName)
			return tracker.images[_viewName];		
		return tracker.images[tracker.viewName];
	}
	
	tracker.getImage = function() {
		return tracker.images[tracker.viewName].shift();
	}
	
	tracker.imgLen = function() {
		return tracker.images[tracker.viewName].length - 1;
	}
	
	tracker.empty = function() {
		return tracker.images[tracker.viewName].length == 0;
	}
	
	return tracker;
}
