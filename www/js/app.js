var sources = {
	'imgur': {
		api: "https://api.imgur.com/3/",
		imgUrl: "http://i.imgur.com/",
		newImg: "user",
		hot: "hot"
	},
	'500px': {
		api: "",
		imgUrl: "",
		newImg: "",
		hot: ""
	},
	'deviantart': ""
}


var share = function(_image) {
	extras = {};
	extras[window.plugins.webintent.EXTRA_TEXT] = _image;
	
	window.plugins.webintent.startActivity({
		action: window.plugins.webintent.ACTION_SEND,
		extras: extras,
		type: 'text/plain'
	}, 
	function() {
	}, 
	function() {
		console.log("Failed to share link")
	}
	);
}



// PIXQ

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('PIXQ', ['ngCordova', 'ionic', 'ionic.contrib.ui.tinderCards'])

//routing
.config(function($stateProvider, $urlRouterProvider, $cordovaAppRateProvider) {
	
	$stateProvider
	.state('index', {
		url: '/new',
		templateUrl: function($stateParams) {
			return 'views/cardStack.html';
		},
		resolve: {
			images: function($q, newImages, userData, pageTracker) {
				var deferred = $q.defer();
				var screen = "newImg";

				/*if (pageTracker.getImages(screen))
				{
						deferred.resolve(false);
						return deferred.promise;
				}*/
				
				newImages.setStartDate(userData, screen);
				newImages.setEndDate(userData.getActiveDate(screen), screen);
				
				newImages.getImages(screen, userData).then(function(_images) {
					deferred.resolve(_images);
				})
				return deferred.promise;
			
			}
		},
		data: {
			viewName: "newImg"
		},
		controller: "CardsCtrl",
		onEnter: function(pageTracker, $cordovaGoogleAnalytics) {
			pageTracker.viewName = "newImg";
			document.addEventListener("deviceready", function() {
				$cordovaGoogleAnalytics.trackView('New');
			})
		},
		onExit: function() {
			console.log("leave");
		}
	})
	.state('hot', {
		url: '/hot',
		templateUrl: function($stateParams) {
			return 'views/cardStack.html';
		},
		resolve: {
			images: function($q, newImages, userData, pageTracker) {
				var _out;
				var screen = "hot";
				var deferred = $q.defer();
				
				/*if (pageTracker.getImages(screen))
				{
						deferred.resolve(false);
						return deferred.promise;
				}*/
				
				newImages.setStartDate(userData, screen);
				newImages.setEndDate(userData.getActiveDate(screen), screen);
				
				newImages.getImages(screen, userData).then(function(_images) {
					deferred.resolve(_images);
				})
				return deferred.promise;
			
			}
		},
		data: {
			viewName: "hot"
		},
		controller: "CardsCtrl",
		onEnter: function(pageTracker, $cordovaGoogleAnalytics) {
			pageTracker.viewName = "hot";
			document.addEventListener("deviceready", function() {
				
				$cordovaGoogleAnalytics.trackView('Hot');
			})
		},
		onExit: function() {
			console.log("leave");
		}
	
	})
	.state('favourites', {
		url: '/favourites',
		templateUrl: function($stateParams) {
			return 'views/favouritesList.html';
		},
		onEnter: function($cordovaGoogleAnalytics) {
			document.addEventListener("deviceready", function() {
				
				$cordovaGoogleAnalytics.trackView('Favourites List');
			})
		},
	})
	.state('favourite', {
		url: '/favourite',
		templateUrl: 'views/favourite.html',
		controller:function($scope, pageTracker){
			$scope.activeFav = pageTracker.activeFav;
			$scope.imageInfo.title = pageTracker.imageInfo.title;
			$scope.imageInfo.description = pageTracker.imageInfo.description;
			$scope.imageInfo.link = pageTracker.imageInfo.link;

			$scope.pressShare = function() {
				share($scope.activeFav.source);
			}			
		},
		onEnter: function($cordovaGoogleAnalytics) {
			document.addEventListener("deviceready", function() {
				
				$cordovaGoogleAnalytics.trackView('Favourite');
			})
		}
	});
	$urlRouterProvider.otherwise("/new");
	
	
	document.addEventListener("deviceready", function() {
		
		var prefs = {
			language: 'en',
			appName: 'PIXQ',
			androidURL: 'market://details?id=com.throwawaygames.pixq',
		};
		
		$cordovaAppRateProvider.setPreferences(prefs)
	
	}, false);

})


.factory(factories)

.run(function($ionicPlatform, $ionicPopup) {
	$ionicPlatform.ready(function() {
		// Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
		// for form inputs)
		if (window.cordova && window.cordova.plugins.Keyboard) {
			cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
		}
		if (window.plugins && window.plugins.AdMob) {
			//var admob_key = device.platform == "Android" ? "ANDROID_PUBLISHER_KEY" : "IOS_PUBLISHER_KEY";
			var admob_key = "ca-app-pub-3772685581108981/8377422552";
			var admob = window.plugins.AdMob;
			admob.createBannerView(
			{
				'publisherId': admob_key,
				'adSize': admob.AD_SIZE.BANNER,
				'bannerAtTop': false
			}, 
			function() {
				admob.requestAd(
				{}, 
				function() {
					admob.showAd(true);
				}, 
				function() {
					console.log('failed to request ad');
				}
				);
			}, 
			function() {
				console.log('failed to create banner view');
			}
			);
		}
		if (window.StatusBar) {
			StatusBar.styleDefault();
		}
	});


})

.controller(controllers);



