console.log = function(){};

var controllers = {};

controllers.Main = function($scope, $state, $ionicSideMenuDelegate, $ionicScrollDelegate, $ionicPopup, 
$timeout, $cordovaAppRate, $ionicModal, userData, $cordovaDevice, $cordovaGoogleAnalytics) {
	
	
	
	document.addEventListener("deviceready", function() {
		var uuid = $cordovaDevice.getUUID();	
		$cordovaGoogleAnalytics.startTrackerWithId('UA-62246305-2');
		$cordovaGoogleAnalytics.setUserId('uuid');
	})

	userData.setActiveDate("hot", 0);



	// An alert dialog
	$scope.showSwipeAlert = function() {
		var alertPopup = $ionicPopup.alert({
			title: 'Welcome to PIXQ!',
			template: 'Swipe left or press X to skip a picture, swipe right or press the checkmark to favourite a picture!'
		});
		alertPopup.then(function(res) {
			if (window.localStorage['swipeTut'] == undefined)
				window.localStorage['swipeTut'] = true;
		});
	};


	//initial swipe tutorial
	if (window.localStorage['swipeTut'] == undefined)
		$scope.showSwipeAlert();
	
	$scope.options = [
		{click: "shareApp",title: "Share this app"}, 
		{click: "rate",title: "Rate this app"}, 
		{click: "about",title: "About & Help"}
	];
	$scope.imageInfo = [];
	$scope.favs = []; //stored images
	$scope.favs = userData.all(); //load userData
	$scope.curCard = -1;
	
	
	
	
	$scope.toggleInfo = function() {
		var elm = angular.element(document.querySelectorAll(".imageInfo"));
		elm.toggleClass("show")
	}
	
	
	$scope.toggleOptions = function() {
		$ionicSideMenuDelegate.toggleRight();
	};
	
	
	$scope.rate = function() {
		document.addEventListener("deviceready", function() {
			$cordovaAppRate.navigateToAppStore().then(function(result) {
			});
		}, false);
	}
	
	$scope.shareApp = function() {
		share("http://play.google.com/store/apps/details?id=com.throwawaygames.pixq");
	}
	
	$scope.about = function() {
		$scope.openModal();
	}
	
	
	$ionicModal.fromTemplateUrl('views/about.html', {
		scope: $scope,
		animation: 'slide-in-up'
	}).then(function(modal) {
		$scope.modal = modal;
	});
	$scope.openModal = function() {
		$scope.modal.show();
	};
	$scope.closeModal = function() {
		$scope.modal.hide();
	};
	//Cleanup the modal when we're done with it!
	$scope.$on('$destroy', function() {
		$scope.modal.remove();
	});
	// Execute action on hide modal
	$scope.$on('modal.hidden', function() {
	// Execute action
	});
	// Execute action on remove modal
	$scope.$on('modal.removed', function() {
	// Execute action
	});
	
	document.addEventListener("click", function(e) {
		var element = e.target;
		if (element.className.indexOf("imgSource") != -1 || 
		element.className.indexOf("company") != -1) {
			e.preventDefault();
			window.open(element.href, "_system", "location=yes");
			return false;
		}
	}, false);

}

controllers.CardsCtrl = function($scope, $rootScope, $http, $timeout, $ionicScrollDelegate, $ionicPopup, 
$cordovaNetwork, TDCardDelegate, userData, newImages, pageTracker, $q, images, $state) {
	
	var direction = null;
	var preloadedImages = []; //ready to use images
	$scope.cards = [];
	$scope.viewName = $state.current.data.viewName;
	pageTracker.viewName = $scope.viewName;
	pageTracker.setFirstLoad(true); //is this the first load
	
	var preloadNum = 0; //preload counter
	
	var addFavourite = function(_image) {
		userData.newFavourite(_image).then(function(_fav) {
			$scope.favs.unshift(_fav);
			userData.save($scope.favs);
		});
	
	}


	// An alert dialog
	$scope.showHotAlert = function() {
		var alertPopup = $ionicPopup.alert({
			title: "Check out some HOT images",
			template: 'Click the Hot tab to view images based on how popular they are'
		});
		alertPopup.then(function(res) {
			if (window.localStorage['hotTut'] == undefined)
				window.localStorage['hotTut'] = 0;
		});
	};


	// An alert dialog
	$scope.showFavAlert = function() {
		var alertPopup = $ionicPopup.alert({
			title: "See your favourite images again",
			template: 'View your saved favourites from the Favs tab'
		});
		alertPopup.then(function(res) {
			if (window.localStorage['favTut'] == undefined)
				window.localStorage['favTut'] = 0;
		});
	};

	// An alert dialog
	$scope.showZoomAlert = function() {
		var alertPopup = $ionicPopup.alert({
			title: "Come a little bit closer!",
			template: 'Double tap a picture to zoom in or out'
		});
		alertPopup.then(function(res) {
			if (window.localStorage['zoomTut'] == undefined)
				window.localStorage['zoomTut'] = 0;
		});
	};
	
	$scope.showNetAlert = function() {
		var alertPopup = $ionicPopup.alert({
			title: "No Internet Connection",
			template: 'Please check your connection'
		});
		alertPopup.then(function(res) {
			if (window.localStorage['tutorials'] == undefined)
				window.localStorage['tutorials'] = 0;
			else if (window.localStorage['tutorials'] < 3)
				window.localStorage['tutorials']++;
			console.log(res);
		});
	};
	
	
	var setupImages = function(_images) {
		
		if (!_images)
				return;
		
		pageTracker.setImages(_images);
		
		if (!newImages.getStartDate($scope.viewName)) 
		{
			userData.setStartDate($scope.viewName, pageTracker.getImages()[pageTracker.imgLen()].datetime);
			newImages.setStartDate(userData, $scope.viewName);
		}
		
		
		if (userData.getActiveDate($scope.viewName) == 0)
			userData.setActiveDate($scope.viewName, pageTracker.getImages()[pageTracker.imgLen()].datetime);
		newImages.setEndDate(pageTracker.getImages()[pageTracker.imgLen()].datetime, $scope.viewName);
		
		preload(5);
	//preload(Math.min(5, pageTracker.imgLen()))
	
	}
	
	
	
	$rootScope.$on("$stateChangeStart", function(event, toState, toParams, fromState, fromParams) {
		
		
		if ($scope.cards == undefined || !$scope.cards.length || $scope.curCard < 0)
			return;
		
		$scope.imageInfo.title = $scope.cards[$scope.curCard].title;
		$scope.imageInfo.description = $scope.cards[$scope.curCard].description;
		$scope.imageInfo.link = $scope.cards[$scope.curCard].source;
	})

	//init, load start and end dates
	
	
	
	
	
	$scope.refresh = function() {
		
		newImages.getImages($scope.viewName, userData).then(function(_images) {
			setupImages(_images)
		})
	}

	//preload images
	var item = null;
	var preload = function(_num) {
		
		
		preloadNum = _num;
		if (pageTracker.empty() || preloadedImages.length > 5) 
		{
			firstload().then(function(_firstLoad) {
				pageTracker.setFirstLoad(_firstLoad);
			});
			return;
		}
		//console.log("preload");
		item = pageTracker.getImage();
		
		if (item.is_album !== undefined && item.is_album)  //skip albums
		{
			preload(preloadNum);
			return;
		}
		
		if (item.animated)
			preload(preloadNum);
		else 
		{
			$timeout(function() { //async load images
				//if (item.type !== "image/gif")
				//img = new Image();
				//img.onload = function(){
				preloadedImages.push(item);
				item = null;
				if (preloadNum) 
				{
					preloadNum--;
					preload(preloadNum);
				} 
				else 
				{
					firstload().then(function(_firstLoad) {
						pageTracker.setFirstLoad(_firstLoad);
					});
				}
			//}
			//img.src = item.image;
			
			}, 0);
		}
	}
	
	
	function firstload() 
	{
		var deferred = $q.defer();
		
		$timeout(function() {
			if (!pageTracker.getFirstLoad())
				deferred.reject();
			else 
			{
				updateCards(5, true);
				
				var _num = $scope.cards.length - 1;
				
				if (_num < 0)
					return;
				if (!$scope.imageInfo.length) 
				{
					//if ($scope.cards[_num].title !== null)
					$scope.imageInfo.title = $scope.cards[_num].title;
					//if ($scope.cards[_num].description !== null)
					$scope.imageInfo.description = $scope.cards[_num].description;
					//if ($scope.cards[_num].source !== null)
					$scope.imageInfo.link = $scope.cards[_num].source;
					$scope.$apply();
				}
				deferred.resolve(false);
			}
		})
		return deferred.promise;
	
	}
	
	
	setupImages(images)
	
	
	var updateCards = function(_num, _add) {
		if (preloadedImages.length <= 0)
			return;
		
		_num = Math.min(preloadedImages.length - 1, _num - 1);
		for (var x = _num; x >= 0; x--) 
		{
			
			var qimage = preloadedImages.shift()
			
			if (qimage.animated) 
			{
				x++;
				continue;
			}
			
			if (_add)
				$scope.curCard = Math.max($scope.curCard, _num);
			
			var index = _add ? x : $scope.curCard;
			console.log(index)
			
			
			$scope.cards[index] = {
				_id: qimage._id,
				cardImage: qimage.cardImage,
				image: qimage.image, //"http://i.imgur.com/"+image.id+"l.jpg",
				thumb: qimage.thumb,
				title: qimage.title,
				description: qimage.description,
				source: qimage.source,
				datetime: qimage.datetime,
				startZ: 8000 + x,
				zoom: false
			};
			
			qimage = null;
		
		}
	}
	
	var nextCard = function(_num) {
		if (pageTracker.empty()) //are we out of images?  get more from the server
			newImages.getImages($scope.viewName, userData).then(function(_images) {
				setupImages(_images)
			})
		else if (preloadedImages.length < 3) //do we need to preload more images?  go do that
			preload(1);


		//update current card index
		if ($scope.curCard > 0)
			$scope.curCard--;
		else
			$scope.curCard = $scope.cards.length - 1;
		
		var cardStack = document.querySelectorAll("." + $scope.viewName + " td-card");
		
		cardStack[$scope.curCard].style.display = "";
		if ($scope.curCard == 0)
			cardStack[cardStack.length - 1].style.display = "";
		else
			cardStack[$scope.curCard - 1].style.display = "";
	}
	
	
	
	
	$scope.cardDestroyed = function() {
		
		
		var num = $scope.cards.length - 1;
		var tmpcard = document.querySelectorAll("." + $scope.viewName + " td-card")[$scope.curCard];
		tmpcard.style.zIndex -= num + 1;
		tmpcard.style.display = "none";
		
		if (direction)
			addFavourite($scope.cards[$scope.curCard]);
		
		userData.setActiveDate($scope.viewName, $scope.cards[$scope.curCard].datetime);


		//if ($scope.cards.length <= 2) 
		//{
		updateCards(1, false);
		//}
		
		nextCard(num);
		if (num >= 0) 
		{
			//$scope.imageInfo = [];
			$scope.imageInfo.title = $scope.cards[$scope.curCard].title;
			$scope.imageInfo.description = $scope.cards[$scope.curCard].description;
			$scope.imageInfo.link = $scope.cards[$scope.curCard].source;
		
		}
	};
	
	$scope.cardSwiped = function() {
		if ($cordovaNetwork.isOffline())
			$scope.showNetAlert();
		else if (window.localStorage['swipeCount'] == undefined)
			window.localStorage['swipeCount'] = 1;
		else if (window.localStorage['swipeCount'] <= 15) 
		{
			window.localStorage['swipeCount']++;
			if (window.localStorage['swipeCount'] == 5)
				$scope.showHotAlert();
			if (window.localStorage['swipeCount'] == 10)
				$scope.showZoomAlert();
			if (window.localStorage['swipeCount'] == 15)
				$scope.showFavAlert();
		}
	}
	
	$scope.cardSwipedLeft = function() {
		direction = 0;
		$scope.cardDestroyed();
	
	};
	$scope.cardSwipedRight = function(index) {
		direction = 1;
		$scope.cardDestroyed();
	};
	
	$scope.pressNext = function() {
		$scope.cardSwipedLeft();
	}
	
	$scope.pressFav = function(card) {
		$scope.cardSwipedRight(card);
	}
	
	$scope.pressShare = function(_index) {
		share($scope.cards[$scope.curCard].source);
	}
	
	$scope.toggleZoom = function(_index) {
		console.log($scope.cards[_index].zoom)
		$scope.cards[_index].zoom = !$scope.cards[_index].zoom;
		if ($scope.cards[_index].zoom)
			$ionicScrollDelegate.zoomTo(2, true);
		else
			$ionicScrollDelegate.zoomTo(0, true);
	}
}

controllers.favList = function($scope, $state, $ionicScrollDelegate, userData, pageTracker) {

	// Grab the last active, or the first favourite.  Not sure if needed
//	pageTracker.activeFav = $scope.favs[userData.getLastActiveFavourite()];
	var zoom = false;
	// Called to select the given project
	$scope.viewFavourite = function(_image, _index) {
		pageTracker.activeFav = _image;
		userData.setLastActiveFavourite(_index);
		
		pageTracker.imageInfo.title = _image.title;
		pageTracker.imageInfo.description = _image.description;
		pageTracker.imageInfo.link = _image.source;

		//$scope.activeFav = $scope.favs[userData.getLastActiveFavourite()];
		$state.go('favourite');
	
	};
	
	$scope.pressShare = function(_index) {
			share($scope.favs[_index].source);
	}
	
	$scope.toggleZoom = function(_index) {
		zoom = !zoom;
		if (zoom)
			$ionicScrollDelegate.zoomTo(2, true);
		else
			$ionicScrollDelegate.zoomTo(0, true);
	}
}

/*
function getWatchers(root) {
  root = angular.element(root || document.documentElement);
  var watcherCount = 0;
 
  function getElemWatchers(element) {
    var isolateWatchers = getWatchersFromScope(element.data().$isolateScope);
    var scopeWatchers = getWatchersFromScope(element.data().$scope);
    var watchers = scopeWatchers.concat(isolateWatchers);
    angular.forEach(element.children(), function (childElement) {
      watchers = watchers.concat(getElemWatchers(angular.element(childElement)));
    });
    return watchers;
  }
  
  function getWatchersFromScope(scope) {
    if (scope) {
      return scope.$$watchers || [];
    } else {
      return [];
    }
  }
 
  return getElemWatchers(root);
}
*/